<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
  /**
   * Seed the users table with example data from brief.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->insert([
      [
        'name' => 'Dave Clark',
        'email_address' => 'dave.clark@intelligencefusion.co.uk'
      ],
      [
        'name' => 'Patricia Summer',
        'email_address' => 'patricia.summer@intelligencefusion.co.uk'
      ],
      [
        'name' => 'Thomas Jeffrey',
        'email_address' => 't.j@intelligencefusion.co.uk'
      ]
    ]);
  }
}