<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentsTableSeeder extends Seeder
{
  /**
   * Seed the comments table with example data from brief.
   *
   * @return void
   */
  public function run()
  {
    DB::table('comments')->insert([
      [
        'user_id' => 1,
        'game_id' => 1,
        'comment' => 'Looks better than the original but isn\'t as good. Not worth the money.'
      ],
      [
        'user_id' => 3,
        'game_id' => 1,
        'comment' => 'Was hoping for something more.'
      ],
      [
        'user_id' => 2,
        'game_id' => 4,
        'comment' => 'Amazing. Can\'t get enough of Star Wars, and this is the best installment yet.'
      ],
    ]);
  }
}