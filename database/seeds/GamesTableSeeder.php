<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamesTableSeeder extends Seeder
{
  /**
   * Seed the games table with example data from brief.
   *
   * @return void
   */
  public function run()
  {
    DB::table('games')->insert([
      [
        'name' => 'Call of Duty: Modern Warfare',
        'publisher' => 'Activision',
        'release_date' => '2019-10-25 00:00:00',
        'encryption_key' => 'cod.master_1234'
      ],
      [
        'name' => 'Code Vein',
        'publisher' => 'Bandai Namco Entertainment',
        'release_date' => '2019-09-27 01:00:00',
        'encryption_key' => 'master_bandai_483'
      ],
      [
        'name' => 'Gears 5',
        'publisher' => 'Xbox Game Studios',
        'release_date' => '2019-09-10 00:00:00',
        'encryption_key' => 'msoft_5gear_key_1'
      ],
      [
        'name' => 'Star Wars Jedi: Fallen Order',
        'publisher' => 'Electronic Arts',
        'release_date' => '2019-11-15 02:00:00',
        'encryption_key' => 'eagames_skeleton_key'
      ],
      [
        'name' => 'FIFA 20',
        'publisher' => 'Electronic Arts',
        'release_date' => '2019-09-27 03:00:00',
        'encryption_key' => 'eagames_skeleton_key'
      ]
    ]);
  }
}
