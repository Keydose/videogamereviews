<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  public function run()
  {
    $this->call([
      GamesTableSeeder::class,
      UsersTableSeeder::class,
      CommentsTableSeeder::class
    ]);
  }
}