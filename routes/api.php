<?php
use Illuminate\Http\Request;

Route::get('/games/{game}/comments', 'GamesController@showComments');
Route::get('/games/{game}', 'GamesController@show');
Route::get('/games', 'GamesController@index');

Route::get('/users/{user}/comments', 'UsersController@showComments');
Route::get('/users/{user}', 'UsersController@show');
Route::get('/users', 'UsersController@index');

Route::get('/comments/{comment}/game', 'CommentsController@showGame');
Route::get('/comments/{comment}/user', 'CommentsController@showUser');
Route::get('/comments/{comment}', 'CommentsController@show');
Route::get('/comments', 'CommentsController@index');