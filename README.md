## You Need

- Composer
- Laravel
- PHP
- A MySQL database

## Information

I created the app with Laravel Homestead but the app can, of course, be ran with a default typical installation of Laravel and artisan as stated in the brief PDF. 

## Running Instructions

Rename the .env.example file in the root directory of the project to .env and adjust the DB_ settings to point to a valid (empty) MySQL database and table, and to use a user with at least full read access.

Run 'composer dump-autoload' from the commandline scoped to the root directory to ensure an up-to-date class map.

With a command window open in the root directory of the project, run 'artisan migrate' to create all the tables for the project, then run 'artisan db:seed' to populate them with the example data provided in the brief.

Run 'artisan serve' from the command line
