<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Comment extends Model
{
  public function user() {
    return $this->belongsTo('App\User');
  }

  public function game() {
    return $this->belongsTo('App\Game');
  }
}