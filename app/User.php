<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class User extends Model
{
  public function comments()
  {
    return $this->hasMany('App\Comment');
  }
}