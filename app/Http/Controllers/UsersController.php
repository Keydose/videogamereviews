<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController
{
  public function showComments(User $user)
  {
    return $user->comments()->get();
  }

  public function show(User $user)
  {
    return $user;
  }

  public function index(Request $request)
  {
    $users = User::select('id', 'name');

    if ($request->has('name_contains'))
    {
      $users = $users->where('name', 'LIKE', '%'.$request->input('name_contains').'%');
    }

    return $users->get();
  }
}