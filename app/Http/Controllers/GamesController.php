<?php
namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;

class GamesController
{
  public function showComments(Game $game)
  {
    return $game->comments()->get();
  }

  public function show(Game $game)
  {
    return $game;
  }

  public function index(Request $request)
  {
    $games = Game::select('id', 'name', 'publisher', 'release_date', 'created_at', 'updated_at');

    if ($request->has('name_contains'))
    {
      $games = $games->where('name', 'LIKE', '%'.$request->input('name_contains').'%');
    }

    if ($request->has('publisher_contains'))
    {
      $games = $games->where('publisher', 'LIKE', '%'.$request->input('publisher_contains').'%');
    }

    if ($request->has('release_date'))
    {
      // Allow the user to provide the date in any format as it will be converted to the MySQL native format before filtering
      $games = $games->where('release_date', 'LIKE', '%'.date('Y-m-d', strtotime($request->input('release_date'))).'%');
    }

    return $games->get();
  }
}