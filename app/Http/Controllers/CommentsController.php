<?php
namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentsController
{
  public function showGame(Comment $comment)
  {
    return $comment->game()->get();
  }

  public function showUser(Comment $comment)
  {
    return $comment->user()->get();
  }

  public function show(Comment $comment)
  {
    return $comment;
  }

  public function index(Request $request)
  {
    $comments = Comment::query();

    if ($request->has('game_id'))
    {
      $comments = $comments->where('game_id', '=', $request->input('game_id'));
    }

    if ($request->has('user_id'))
    {
      $comments = $comments->where('user_id', '=', $request->input('user_id'));
    }

    return $comments->get();
  }
}