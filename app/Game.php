<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Game extends Model
{
  // Represents date in the format of 12am UTC, 25th October 2019, for example.
  protected $dateFormat = 'ga e, jS F Y';

  // Mutator so that when grabbing the date via the API it's in the format provided in the example
  public function getReleaseDateAttribute($value)
  {
    return date('ga e, jS F Y', strtotime($value));
  }

  public function comments()
  {
    return $this->hasMany('App\Comment');
  }
}